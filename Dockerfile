FROM node:10

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app
EXPOSE 5000

ENV ENV="development"
ENV DB_HOST="localhost"
ENV PORT="5000"
ENV URI="mongodb://kmiloarguello:xQDxrDjJzHPSO71ln9pZ@ds155747.mlab.com:55747/conflux-dev"
ENV OR_KEY="A8m39A3QpgKM6I60"
ENV ACT_DB="Active Database: ConfluxDB-dev"
ENV API_KEY="ZpqIwWpZHnJq6wwJL6hdPz5n4wVwXaZg"
ENV CLIENT="http://localhost:4200"
ENV GOOGLE_ID="289240002840-ntk6b0dckjln43k26kg4r7o2qmss1f68.apps.googleusercontent.com"
ENV GOOGLE_SECRET="LvEVr4RDOemThy4Cp01cddoy"
ENV FACEBOOK_APP_ID="694681567973378"
ENV FACEBOOK_APP_SECRET=""
ENV TOKEN_EXPIRATION_TIME_SECONDS="3000"

CMD [ "npm", "start" ]