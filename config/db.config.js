const mongoose = require('mongoose');
const MONGODB_URI = process.env.ENV === 'development' ? process.env.URI : process.env.URI;
// DB config
const {ACT_DB, URI} = process.env;

mongoose.Promise = global.Promise
console.log(`Preparing ${process.env.ENV} connection 💻`)
mongoose
  .connect(URI, { 
    connectTimeoutMS: 5000,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }) 
  .then(() => console.log(`🌩  Connected to de database 🗄️, ${ACT_DB}`))
  .catch(console.error);
