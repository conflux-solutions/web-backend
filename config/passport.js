const JwtStrategy = require('passport-jwt').Strategy;
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const Owner = mongoose.model('Owner');
const Users = mongoose.model('Users');
// const config = require('../utils/config');
const passport = require('passport');
const Keys = require('../config/keys');

//This verifies that the token sent by the user is valid
passport.use(new JwtStrategy({
    //secret we used to sign our JWT
    secretOrKey : Keys.secretOrKey,
    //we expect the user to send the token as a query paramater with the name 'secret_token'
    jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken()
  },(token, done) => {
    const modelValidationById = modelValidation({ _id: token._id })
    // console.log(`JWT strategy 👮  ${JSON.stringify(token)}`)
    return /owner/ig.test(token.type)
      ? done(null, modelValidationById(Owner))
      : done(null, modelValidationById(Users))
  })
);

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_ID,
  clientSecret: process.env.GOOGLE_SECRET,
  callbackURL: "/auth/google/callback",
  passReqToCallback: true
}, socialLoginHandler));

passport.use(new FacebookStrategy({
  clientID: process.env.FACEBOOK_APP_ID,
  clientSecret: process.env.FACEBOOK_APP_SECRET,
  callbackURL: "/auth/facebook/callback",
  passReqToCallback: true,
  profileFields: ['id', 'email', 'gender', 'name']
}, socialLoginHandler));

// ? we can export this function
// Validation whether the user owner exists or not
const modelValidation = params => Model =>
  Model.findOne(params) //* usually it fill find by id. _id, googleId or facebookId
  .then(data => data || false)
  .catch(console.error); 


// ! this should be in auth.js file
async function socialLoginHandler (req, accessToken, refreshToken, {_json}, done) {
  console.log(`👮🏻‍♂️ Passport 🎫 ${req.session.strategy.toUpperCase()} strategy for ${req.session.type}`)
  // Defining the model to work with
  //? logic error. For no owner session type the model will be User.
  const Model = new RegExp('owner','i').test(req.session.type) ? Owner : Users;

  // Search for a user/owner or create a new one
                // * look by email
  const user =  await modelValidation({ email: _json.email })(Model) ||
                //* create a new user
                // ! test if facebook strategy return the same json object than google
                await new Model({
                  user_name: _json.name,
                  email: _json.email,
                  [`${req.session.type}ID`]: _json.sub, //! check!!
                  ...req.session.type === 'owner' && {locale: _json.locale},
                }).save()
  // always return an user.
  return done(null, user)
}