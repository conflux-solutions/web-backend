
// DEBUG CA
// module.exports = {
//     mongoURI: "mongodb://localhost:27017/conflux",
//     secretOrKey: "confluxKey",
//     activeDb: "Active Database: ConfluxDB",
//     mapQuestAPIKey: "ZpqIwWpZHnJq6wwJL6hdPz5n4wVwXaZg"
// }
// PRODUCTION KEYS
console.log(process.env.URI)
module.exports = {
    mongoURI: process.env.URI,
    secretOrKey: process.env.OR_KEY,
    activeDb: process.env.ACT_DB,
    mapQuestAPIKey: process.env.API_KEY
};