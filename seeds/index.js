
require('dotenv').config();
const what = process.env.npm_config_what        // npm -what=users run seeds
const who = process.env.npm_config_who          // npm -who=12345678 run seeds
const how = process.env.npm_config_how          // npm -how=home run seeds
//* Import or generate the data
require('../config/db.config');
const {createModel, fn:{Close, emptyArray}} = require('./utils')
//* import models 
// Models
const newOwner = createModel(require("../models/Owner"));
const newCategory = createModel(require("../models/Category"));
const newOpenHours = createModel(require("../models/Opening_hours"));
const newEstablishement = createModel(require("../models/Establishment"));

const {userMail, department, rangeHours, establishment} = require('./mockData')

console.log(`ready to import ${process.env.npm_config_many}(s) ${what} 📦 `)

//? owners:
if(what === 'owners') {
  newOwner(userMail).then(Close)
}

//? categories 
if(what === 'category') {
  newCategory(department).then(Close)
}

//? openHours 
if(what === 'openHours') {
  newOpenHours(rangeHours).then(Close)
}

//? establecimientos
if(what === 'establishement') {
  (async function (loop) {

    const openHours = await newOpenHours(rangeHours)
    const categories = how || await newCategory(department)
    const owners = who || await newOwner(userMail)
    const nArray = emptyArray(parseInt(loop))       // process.env.npm_config_many

    newEstablishement(nArray((_, i )=> ({
      ...establishment[i],
      id_opening_hours: openHours[i]._id,
      id_category: how || categories[i]._id,
      id_owner: who ||  owners[i]._id
    })))
      .then(Close)

  }(process.env.npm_config_many || 1));
  
}