require('dotenv').config();
const faker = require('faker');
const {sampleSize, sample} = require('lodash');
const {fn:{random7, coords, emptyArray}} = require('./utils');

const howMany = process.env.npm_config_many || 1// npm -what=users --many=2 run seeds
const nArray = emptyArray(parseInt(howMany))

const userMail = nArray(_=>({
  user_name: faker.name.firstName(),
  email: faker.internet.email(),
}))

const department = nArray(_=>({
  name:faker.commerce.department()
}))

const rangeHours = nArray((_=>({
  day: emptyArray(random7())(_=>random7()),
  open_hour: faker.date.between('2020-06-01T07:00:12', '2020-06-01T09:10:12'),
  close_hour: faker.date.between('2020-06-01T17:00:12', '2020-06-01T19:10:12'),
})))

const establishment = nArray((_=>({
  name: faker.company.companyName(),
  phone: faker.phone.phoneNumber(),
  photo: faker.image.business(),
  description: faker.lorem.paragraph(),
  isActive: faker.random.boolean(),
  current_affluences: faker.random.number({min:10, max:500}),
  max_affluences_allowed: faker.random.number({min:5, max:500}),
  shift_attention_mins: faker.random.number({min:10, max:30}), // Tiempo promedio en atención | slot 30min ... # personas que se atienden en 30 min ??
  shift_schedule_max_hours: faker.random.number({min:24, max:50}), // --
  checkin_max_min: faker.random.number({min:10, max:20}), // Máximo numero de tolerancia a puntualidad i.e 10min
  max_shifts: faker.random.number({min:1, max:50}), // Hasta cuando permite agendar
  max_persons_per_slot: faker.random.number({min:10, max:50}),
  location: {
    //* Range of Lat & Long from France
    //? https://www.mapsofworld.com/lat_long/france-lat-long.html
    latitude: coords(50, 41),
    longitude: coords(9,0),
    // 💩 this data is very random...
    address: faker.address.streetAddress(),
    city: faker.address.city(),
    stateCode: faker.address.stateAbbr(),
    postalCode: faker.address.zipCode(),
  },
})))




module.exports = {
  userMail,
  department,
  rangeHours,
  establishment
}