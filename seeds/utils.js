const mongoose = require('mongoose');
const faker = require('faker');
const emptyArray = n => callback => new Array(n).fill(null).map(callback)

function createModel(Model) {
  // 🍛
  return (params, disconnect = true) => {
    // console.log(`👕 `, Model)
    // console.log(`🍛  ${JSON.stringify(params)}`)
    return new Promise((resolve, reject) => {
      Model.create(params)
        .then(allOk)          // log
        .then(resolve)        // return
        // .then(Close)          // disconnect mongo
        .catch(err => {
          // console.error('💩 ', err)
          reject(err)
          Close(`🕵️‍♂️ error in the Model creation`)
        })
    })
  }
}

function random7 () { //we can currify this
  return Math.round(faker.random.number({'max': 6}))
}

const coords = (max, min, precision = 4) => faker.random.number({
  max, min,
  precision: parseFloat((0.0).toPrecision(precision) + '1')
}).toFixed(precision);

function Close  (msg) {
  msg && console.log(msg)
  console.log(' 🔌  Disconnect mongo ')
  mongoose.connection.close()
}
function allOk (data) {
  console.info(`🎉  ${data.length} elements added to the database`)
  return data
}

module.exports = {
  createModel,
  fn : {
    random7,
    coords,
    Close,
    emptyArray
  }
}